local DeBris = {}
-------------------------------------------------------------------------------

local function equals(field, value)
  return function(row)
     if row == nil then return false end
     if row[field] == nil then return false end
     return row[field] == value
  end
end

local function lessThan(field, value)
  return function(row)
    if row == nil then return false end
    if row[field] == nil then return false end
    return row[field] < value
  end
end

local function lessThanOrEqualTo(field, value)
  return function(row)
    if row == nil then return false end
    if row[field] == nil then return false end
    return row[field] <= value
  end
end

local function greaterThan(field, value)
  return function(row)
    if row == nil then return false end
    if row[field] == nil then return false end
    return row[field] > value
  end
end

local function greaterThanOrEqualTo(field, value)
  return function(row)
    if row == nil then return false end
    if row[field] == nil then return false end
    return row[field] >= value
  end
end

local function in_(field, values)
  local lookup = {}
  for _, value in ipairs(values) do lookup[value] = true end
  return function(row)
    if row == nul then return false end
    if row[field] == nil then return false end
    return lookup[row[field]] or false
  end
end

local function filterAST()
  epnf = require('epnf')
  grammar = epnf.define(function(_ENV)
    -- TODO: add error handling
    SUPPRESS('PAREN','EXPR','BEGIN','VAL')

    local _ = WS^0
    local lt = P('<=') + P('<')
    local gt = P('>=') + P('>')
    local eq = P('=')  + P('!=')
    OP      = C(lt + gt + eq)
    OPIN    = C(P("IN") + P("in"))
    OPBOOL  = C(P('AND') + P('OR') + P('and') + P('or'))
    KEY     = ID + P('{') * C(1-P('}')) * P('}')
    STRING  = CSTRING
    NUM     = C(FLOAT + INT)
    VAL     = V"NUM" + V"STRING"

    START('BEGIN')
    BEGIN   = _*(V"BOOL" + V"EXPR")*_*(-P(1))
    BOOL    = V"EXPR" * _ * (V"OPBOOL" * WS^1 * V"EXPR" * _)^1
    PAREN   = P('(') *_* (V"BOOL" + V"EXPR") *_* P(')') *_
    CMP     = V"KEY"*_*V"OP"*_*V"VAL"*_
    IN      = V"KEY"*_*V"OPIN"*_*V"LIST"*_
    LIST    = P("[")*_*V"VAL"*_*(P(",")*_*V"VAL"*_)^0*P("]")*_
    EXPR    = V"PAREN" + V"CMP" + V"IN"
  end)
  
  local parser = function(str) return epnf.parsestring(grammar, str, "query") end
  return parser
end

-------------------------------------------------------------------------------

function AST2filter(ast)
  
end



--- Factory to create new filters
--
function filter_new(field, op, value)
  local fnc = {["="]=equals, ["<"]=lessThan, ["<="]=lessThanOrEqualTo,
               [">"]=greaterThan, [">="]=greaterThanOrEqualTo, ["in"]=in_}
  if fnc[op] == nil then return nil end
  return fnc[op](field, value)
end

-------------------------------------------------------------------------------

local DB = {}

--- Creates a new database.
-- This creates a new DB object. A table can be passed in where the rows of the
-- table are the intial data items, and any field in the table are stored as metadata.
-- @function DeBris.new
-- @param rows (optional) initial database object containing rows and/or properties.
-- @returns the new database
-- @usage -- An empty database
-- local db1 = DeBris.new()
-- -- a database with a few rows and properties defined
-- local db2 = DeBris.new({ table='kingdoms',
--                         {kingdom='Araluen',ruler='Duncan'},
--                         {kingdom='Scandia',ruler='Ragnak'}})
function DB.new(rows)
  -- A database object has a fields property that stores metadata about the database.
  -- Each data item is stored as a row in the database.
  
  -- Set up the database metadata (store in a fields variable).
  local fields = {}
  if rows and not rows.fields then
    for k,v in pairs(rows) do
      fields[k] = v
    end
  end
  
  local db = rows or {}
  if not db.fields then db.fields = fields end
  setmetatable(db, DB)
  return db
end

--- Lookup for database operations or fields.
-- @function DB:__index
-- @param items the item to look up.
-- @returns the requested database function or field
-- @usage -- Assuming db is a valid database, and 'type' is a database field
-- print(db['type'])
function DB:__index(item)
  value = rawget(self, item) or DB[item] or self:field_(item)
  return value
end

--- Selects items from the database.
-- @function DB:select
-- @param cmd the select command to perform.
-- @returns a new database with the specific desired items selected.
function DB:select(cmd)
  rows = {}
  for i,v in ipairs(self) do
    if v[cmd] then rows[#rows+1] = v end
  end
  return (#rows == 0) and nil or DB.new(rows)
end

--- Return a field from the database.
-- The database itself is checked first for a field. It then looks at the rows in order
-- and returns the first value from the first row that contains the field.
-- @param field_name the name of the field to look up and return.
-- @usage -- Assuming db is a valid database object
-- print(db.field_['name'])
function DB:field_(field_name)
  field = self.fields[field_name]
  if field then return field end
  
  for i, row in ipairs(self) do
    if row.field_ then
      field = row.field_(field_name)
      if field then return field end
    else
      field = row[field_name]
      if field then return field end
    end
  end
  return nil
end

--- Appends the set of items as database rows.
-- @function DB:append
-- @param ... a set of rows to append to the database instance.
-- @returns the database object so that more ops can be performed.
-- @usage local db = DeBris.new()
-- db:append({name='Will', age=14}, {name='Halt', age=45, job='Ranger'})
function DB:append(...)
  -- Add each of the args as db rows.
  for i,v in ipairs({...}) do
    self[#self+1] = v
  end
  return self
end

--- Accepts a table and appends each item in the table as a database row.
-- @function DB:extend
-- @param rows a table containing rows to append to the database instance.
-- @returns the database object so that more ops can be performed.
-- @usage local db = DeBris.new()
-- db:extend( { {name='Will', age=14}, {name='Halt', age=45, job='Ranger'} } )
function DB:extend(rows)
  -- Add each of the args as db rows.
  return self:append(unpack(rows))
end

function DB:groupby()
  
end

function DB:merge()
end

-------------------------------------------------------------------------------

DeBris.new = DB.new
DeBris.filter_new = filter_new
DeBris.filterAST = filterAST()

return DeBris
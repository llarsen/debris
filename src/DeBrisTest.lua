require 'luaspec'
require 'luamock'
DB = require 'DeBris'

context_env_defaults['check_errors'] = false

-------------------------------------------------------------------------------

describe['The filterAST function'] = function(_ENV)
  describe['when [[  Alan = "Level 7"]] is parsed'] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST([[  Alan = "Level 7"]])
    end
    
    it['should get AST in return'] = function(_ENV)
      expect(ast).should_be({{"Alan",id="KEY",pos=3},{"=", id="OP", pos=8},{"Level 7",id="STRING",pos=10}, id="CMP", pos=3})
    end    
  end
  
  describe[ [[when "program='TRON' AND user='ALAN1'" is parsed]] ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST([[program='TRON' AND user='ALAN1']])
    end
    
    it['should return AST structure'] = function(_ENV)
      local program = {{"program",id="KEY",pos=1},{"=", id="OP", pos=8},{"TRON",id="STRING",pos=9}, id="CMP", pos=1}
      local AND     = {"AND", id="OPBOOL", pos=16}
      local user    = {{"user",id="KEY",pos=20},{"=", id="OP", pos=24},{"ALAN1",id="STRING",pos=25}, id="CMP", pos=20}
      expect(ast).should_be({program, AND, user, id="BOOL", pos=1})
    end    
  end

  describe[ [[when "flynn > 'Dillenger'" is parsed]] ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST([[flynn > 'Dillenger']])
    end
    
    it['should get AST in return'] = function(_ENV)
      expect(ast).should_be({{"flynn",id="KEY",pos=1},{">", id="OP", pos=7},{"Dillenger",id="STRING",pos=9}, id="CMP", pos=1})
    end    
  end  
  
  describe[ [[when "TRON_loc='io tower' and ALAN1='logged on' AND disk='weapon'" is parsed]] ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST([[TRON_loc='io tower' and ALAN1='logged on' AND disk='weapon']])
    end
    
    it['should return AST structure'] = function(_ENV)
      local TRON  = {{"TRON_loc",id="KEY",pos=1},{"=", id="OP", pos=9}, {"io tower", id="STRING",pos=10}, id="CMP", pos=1}
      local and_  = {"and", id="OPBOOL", pos=21}
      local ALAN1 = {{"ALAN1",id="KEY",pos=25}  ,{"=", id="OP", pos=30}, {"logged on",id="STRING",pos=31}, id="CMP", pos=25}
      local AND_  = {"AND", id="OPBOOL", pos=43}
      local DISK  = {{"disk",id="KEY",pos=47}   ,{"=", id="OP", pos=51}, {"weapon",   id="STRING",pos=52}, id="CMP", pos=47}
      expect(ast).should_be({TRON, and_, ALAN1, AND_, DISK, id="BOOL", pos=1})
    end    
  end
  
  describe[ [[when "a=4 or b='test' and c=5" is parsed]] ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST("a=4 or  b='test' and c<=5")
    end
    
    it['should return AST structure'] = function(_ENV)
      a   = {{"a",id="KEY",pos=1},{"=", id="OP", pos=2},{"4",id="NUM",pos=3}, id="CMP", pos=1}
      OR  = {"or",id="OPBOOL",pos=5}
      b   = {{"b",id="KEY",pos=9},{"=", id="OP", pos=10},{"test",id="STRING",pos=11}, id="CMP", pos=9}
      AND = {"and",id="OPBOOL",pos=18}
      c   = {{"c",id="KEY",pos=22},{"<=", id="OP", pos=23},{"5",id="NUM",pos=25}, id="CMP", pos=22}
      expect(ast).should_be({a, OR, b, AND, c, id="BOOL", pos=1})
    end    
  end
  
  describe['when "version = 1.0" is parsed' ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST("version = 1.0")
    end
    
    it['should return AST structure'] = function(_ENV)
      expect(ast).should_be({{"version",id="KEY",pos=1},{"=", id="OP", pos=9},{"1.0",id="NUM",pos=11}, id="CMP", pos=1})
    end
  end
  
  describe['when "characters in ["Flynn","Lora","Alan","Dillenger"]" is parsed' ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST([=[characters in ["Flynn","Lora","Alan","Dillenger"] ]=])
    end
    
    it['should return AST structure'] = function(_ENV)
      local CHRACTERS = {"characters", id="KEY", pos=1}
      local IN        = {"in", id="OPIN", pos=12}
      local Flynn     = {"Flynn",     id="STRING", pos=16}
      local Lora      = {"Lora",      id="STRING", pos=24}
      local Alan      = {"Alan",      id="STRING", pos=31} 
      local Dillenger = {"Dillenger", id="STRING", pos=38}
      expect(ast).should_be({CHARACTERS, IN, 
        {Flynn, Lora, Alan, Dillenger, id="LIST", pos=15}, id="IN", pos=1})
    end
  end  
  
  describe['when "memory <= 2.0e6 and time < 1.0e-6" is parsed' ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST("memory <= 2.0e6 and time < 1.0e-6")
    end
    
    it['should return AST structure'] = function(_ENV)
      memory = {{"memory",id="KEY",pos=1},{"<=",id="OP",pos=8},{"2.0e6",id="NUM",pos=11},  id="CMP", pos=1}
      AND    = {"and", id="OPBOOL", pos=17}
      time   = {{"time",id="KEY",pos=21},  {"<",id="OP",pos=26}, {"1.0e-6",id="NUM",pos=28}, id="CMP", pos=21}
      expect(ast).should_be({memory, AND, time, id="BOOL", pos=1})
    end
  end
  
  describe[ [[when "(tron='lost' or ram='injured') and flynn = 'delayed'" is parsed]] ] = function(_ENV)
    before = function(_ENV)
      ast = DB.filterAST("(tron='lost' or ram='injured') and flynn = 'delayed'")
    end
    
    it['should return AST structure'] = function(_ENV)
      tron  = {{"tron",id="KEY",pos=2},  {"=",id="OP",pos=6}, {"lost",id="STRING",pos=7}, id="CMP", pos=2}
      OR    = {"or", id="OPBOOL", pos=14}
      ram   = {{"ram",id="KEY",pos=17},{"=",id="OP",pos=20},{"injured",id="STRING",pos=21},id="CMP",pos=17}
      AND   = {"and", id="OPBOOL", pos=32}
      flynn = {{"flynn",id="KEY",pos=36},{"=",id="OP",pos=42},{"delayed",id="STRING",pos=44},  id="CMP", pos=36}
      expect(ast).should_be({{tron, OR, ram, id="BOOL", pos=2}, AND, flynn, id="BOOL", pos=1})
    end
  end  
end

-------------------------------------------------------------------------------

describe['A DeBris filter'] = function(_ENV)
  describe['when a = filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('kingdom','=','Scandia')
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the kindom property'})).should_be(false)
    end

    it['should return false if the property exists by is not the same'] = function(_ENV)
      expect(filter({kindom='Araluen'})).should_be(false)
    end
    
    it['should return true if the property exists and is equal to Scandia'] = function(_ENV)
      expect(filter({kingdom='Scandia'})).should_be(true)
    end
  end
  
  describe['when a < filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('population','<',10000)
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the population property'})).should_be(false)
    end

    it['should return false if the property exists and is same'] = function(_ENV)
      expect(filter({population=10000})).should_be(false)
    end
    
    it['should return true if the property exists is less than the value'] = function(_ENV)
      expect(filter({population=9999})).should_be(true)
    end
  end

  describe['when a <= filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('population','<=',10000)
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the population property'})).should_be(false)
    end

    it['should return false if the property is greater than the value'] = function(_ENV)
      expect(filter({population=10001})).should_be(false)
    end
    
    it['should return true if the property is equal to value'] = function(_ENV)
      expect(filter({population=10000})).should_be(true)
    end

    it['should return true if the property is less than value'] = function(_ENV)
      expect(filter({population=9999})).should_be(true)
    end
  end

  describe['when a > filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('population','>',10000)
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the population property'})).should_be(false)
    end

    it['should return false if the property is less than the value'] = function(_ENV)
      expect(filter({population=9999})).should_be(false)
    end
    
    it['should return false if the property is equal to value'] = function(_ENV)
      expect(filter({population=10000})).should_be(false)
    end

    it['should return true if the property is greater than value'] = function(_ENV)
      expect(filter({population=10001})).should_be(true)
    end
  end

  describe['when a >= filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('population','>=',10000)
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the population property'})).should_be(false)
    end

    it['should return false if the property is less than the value'] = function(_ENV)
      expect(filter({population=9999})).should_be(false)
    end
    
    it['should return true if the property is equal to value'] = function(_ENV)
      expect(filter({population=10000})).should_be(true)
    end

    it['should return true if the property is greater than value'] = function(_ENV)
      expect(filter({population=10001})).should_be(true)
    end
  end
  
  describe['when an "in" filter is created'] = function(_ENV)
    before = function(_ENV)
      filter = DB.filter_new('number','in',{1,2,3,5})
    end
  
    it['should return false if the parameter is nil'] = function(_ENV)
      expect(filter(nil)).should_be(false)
    end

    it['should return false if the property is not present'] = function(_ENV)
      expect(filter({condition='does not contain the population property'})).should_be(false)
    end

    it['should return false if the property is not in the list'] = function(_ENV)
      expect(filter({number=0})).should_be(false)
      expect(filter({number=4})).should_be(false)
    end
    
    it['should return true if the property is in the list'] = function(_ENV)
      expect(filter({number=1})).should_be(true)
      expect(filter({number=2})).should_be(true)
      expect(filter({number=3})).should_be(true)
      expect(filter({number=5})).should_be(true)
    end
  end
end

-------------------------------------------------------------------------------

describe['A DeBris database'] = function(_ENV)
  before = function(_ENV)
    db = DB.new({tbl_type='People'})
  end
  
  it['should be able to lookup table properties using db[field] syntax'] = function(_ENV)
    expect(db['tbl_type']).should_be('People')
  end
  
  it['should be able to lookup table properties using db.field_(field) syntax'] = function(_ENV)
    expect(db:field_('tbl_type')).should_be('People')
  end  
  
  describe['when items are appended'] = function(_ENV)
    before = function(_ENV)
      will = {name='Will', age=14}
      halt = {name='Halt', age=45, job='Ranger', select='Will'}
      db:append(will, halt)
    end
    
    it["should add these items to the list of database rows"] = function(_ENV)
      expect(db[1]).should_be(will)
      expect(db[2]).should_be(halt)
    end
    
    it['should allow lookup of special field names with field_'] = function(_ENV)
      -- 'select' is a db command, so db['select'] cannot be used to get this field.
      -- db.field_('select') is required for this field
      expect(db:field_('select')).should_be('Will')
    end
    
    describe['when db is extended by a table'] = function(_ENV)
      before = function(_ENV)
        horace = {name="Horace", age=14}
        alyss  = {name="Alyss",  age=14}
        db:extend({horace, alyss})
      end
      
      it["should add the table items individually "] = function(_ENV)
        expect(db[3]).should_be(horace)
        expect(db[4]).should_be(alyss)
      end      

      describe['when a field name is selected'] = function(_ENV)
        before = function(_ENV)
          employed = db:select('job')
        end
        
        it['should return rows with that field defined'] = function(_ENV)
          expect(#employed).should_be(1)
          expect(employed[1]).should_be(halt)
        end
      end
    end
  end
end

spec:report(true)
